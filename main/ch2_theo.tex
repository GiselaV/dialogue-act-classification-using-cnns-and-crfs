\chapter{Theoretical Foundation}\label{theo}
This chapter comprises a deeper description of the theoretical concepts used to approach our problem of modeling \ac{da}s. First, we specify what entails a \ac{da}. After that, we illustrate \acp{cnn} and \ac{crf}, which are the two components that we are proposing in order to predict \ac{da}s. The last section of this chapter goes along the previous work done in this field and how they also played a role inspiring this thesis.

\section{Dialog Acts}
\ac{da}s may trace their origins back to speech act theory in the 60's as \cite{Austin1962} suggested that a sentence is better \textit{used in making a statement} rather than \textit{being always a statement}. It is also interesting to look at \cite{Austin1962}'s work, because he was, from the very beginning, the one saying that distinguishing between commands, questions and so on from statements is not easy and also that it is not a matter of identifying grammatical marks. It is fascinating that half a century later we are trying to teach machines to perform this differentiation. 

In speech act theory, \cite{Austin1962} defined a sentence by taking into account the speaker rather than the meaning itself. He introduced the concept of illocutionary acts by focusing more on the circumstances of a statement, defining it in terms of how it was uttered, as well as the situation in which it occurred. \cite{Austin1962} differentiated among locutionary, illocutionary and perlocutionary. Locutionary is seen as the phonetic aspect of the utterance (what is said). Contrary to that, the illocutionary act has a certain force, we could call it intention (what is meant). In this regard, an utterance might have the force of a question or a command. And the perlocutionary act differs on both and is the effect caused by the utterance. Finally, we can take the classes defined in \cite{Austin1962}'s illocutionary acts as the first step for what we call nowadays \ac{da}s.

But \cite{Austin1962} was not the only one that dealt with speech acts. Later on, \cite{Searle1965} suggested that the basic unit in the communication is constituted by the production of speech acts. He also stated that intention is the essence of performing illocutionary acts \citep{Searle1969}. As a result, intention obtained more relevance through time, illocutionary acts were also defined as the intention of the speaker \citep{Schiffer1972}. However, this time intention takes different values depending on the audience, which leads to different groups. Introducing then a categorization that included four different acts: constatives (e.g. assentives, predictives), directives (e.g. requestives, questions), commissives (e.g. promises, offers) and acknowledgments (e.g. apologize, condole ) \citep{Bach1979}. All these previous research in speech theory opened the door to look closer to dialogues. For instance, it was suggested that not only classification was important, but also the structure of the discourse, and how structure could help to explain interruptions as well as certain expressions \citep{Grosz1986}. All in all, we now know that there is a taxonomy of speech acts and that they also have a structure among them, which are relevant for our research if we think about the structure in discourse involving dialogues too. 

From our perspective, speech theory covers what is meant by the speaker in general, whereas the linguistic structure considers the sequence of utterances and also their intention \citep{Grosz1986}. This idea of linguistic structure brings us to think  that a conversation is performed as a whole and not as a sequence of independent utterances. Therefore, \ac{da}s have been defined as an extension of speech acts, keeping the sense of conveying the intention of the speaker, including an internal structure related to conversational functions \citep{Allen1997}. In general, \ac{da}s can be defined as a set of labels that illustrate the intention of the speaker performing an utterance in a conversation.

\section{Machine Learning}\label{ml}
\ac{ml} is a branch of computer science that deals with bringing machines to act in a specific manner without being explicitly programmed for that \citep{Samuel1959}. In other words, the idea is to stop using rules and heuristics and to let the system learn a function by itself \citep{Doermann2014}. \cite{Bishop1995} explains \ac{ml} by giving the example of image recognition. Imagine we have many pictures of handwritten zip-codes and we want to identify which digit is represented in each image. In order to achieve this task we have different possibilities, one of them is to program a handcrafted rule-based classifier that identifies the shapes and separates them depending on the form.  On the other hand, we can label our data, which will result on a set of images (\textit{input set}) and its tags (\textit{target labels}). Having annotated data, we are able to execute a supervised task (\textit{using annotations}) by creating an adaptive model. This model would be tuned using a set of data (\textit{training set}) and evaluated on a smaller data sample (\textit{test set}). During training, the model produces an output vector with a score for each category, in this case for each number. That vector contains real numbers as scores. The ideal case is to have the highest value for the actual or desired label. The objective in training is to calculate the error between the output predicted by the system and the target labels and then adjust internal parameters to reduce that error, those parameters is what we call \textit{weights} \citep{Lecun2015}.

\subsection{Deep Learning}
Within \ac{ml} we can also find several types of strategies to produce adaptive models. One of them is the field of neural networks or also \textit{re-branded} called \ac{dl} \citep{Goldberg2017}. It comprises a set of methods inspired on how computations work on the human brain, therefore the \textit{neural} part. One of its main characteristics is that it performs learning from differentiable mathematical functions. Furthermore, \ac{dl} models are able to learn highly complex decision boundaries extracting features automatically. Additionally, the \textit{deep} term was assigned due to the several layers of those functions that normally compose a neural network model \citep{Goldberg2017}. In this area of \ac{ml} we also have training and testing processes to adjust our weights. \cite{Lecun2015} report that in a typical \ac{dl} model we might have hundreds of millions of weights and input samples. However, a conventional linear classifier requires hand-crafted feature engineering, the difference with \ac{dl} that it learns features automatically through a general-purpose learning process \citep{Lecun2015}.

\subsection{Convolutional Neural Networks}

In the last decades, neural networks have shown promising and even state-of-the-art performance for different tasks. They have gained some relevance in different research fields, like computer vision \citep{Lecun1995} among others. Especially, \acp{cnn} have demonstrated impressive success in areas like image recognition \citep{Krizhevsky2012} and object detection \citep{Girshick2014}. According to \cite{Schmidhuber2015}, \acp{cnn} were introduced first by \cite{Fukushima1979} as a Neocognitron. However, it was not until \cite{Lecun1990} implemented back-propagation on the top of the Neocognitron that \acp{cnn} gained some popularity. \cite{Lecun1990} attempted to recognize hand-written digits using a back-propagation network that performed convolutions. Due to the origins in computer vision, there are some concepts that were adopted from that field like channels or filters, which are also known as kernels. \acp{cnn} have been recently used also to solve problems in \ac{nlp}. The first one using them was \cite{Collobert2011} to perform semantic role labeling. Later on \cite{Kim2014} attempted sentence classification for sentiment analysis and \cite{Kalchbrenner2013} for discourse compositionality.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.48]{convall}
	\caption[Convolution operation]{Convolution operation sliding a k-word window over the matrix representation of an input, in our case an utterance. A matrix convolution occurs when we slide the kernel, also known as filter, and add the results of an element-wise product of the kernel and the corresponding segment of the utterance matrix. The resulting scalar of this operation is concatenated into a vector that we call \textit{feature map}, illustrated as the output.}
	\label{filter}
\end{figure}

\acp{cnn} are appropriate when trying to identify local features in ordered structure sets. For example, if we consider a sentiment analysis task, there are words that might deliver more information than others. In this regard, looking at expressions like \textit{not bad}, \textit{amazing} or \textit{quite good} might be more efficient contemplating stop words (to, and, etc.). The purpose in this kind of assignment  is to extract that relevant knowledge in an efficient manner. N-gram models might help to solve this problem, but they could also result in a huge embedding matrix \citep{Goldberg2017}. The goal of \acp{cnn} is to produce a fixed length vector representation, by identifying local predictors in a large structure \citep{Goldberg2017}. A convolution should be understood as a mathematical operation of two functions producing a third function.  


In a \ac{cnn} layer each computation unit responds to a small region of input data \citep{Johnson2014}. We can think about it as a stencil or a window sliding over the matrix representation of the input, in our case a sentence. The window is what we will call filter or also kernel. Imagine we have two matrices, one representing a sentence and and another being the filter. The convolution is applied at the moment of sliding the filter matrix over the sentence matrix, while adding the element-wise product of elements in the filter and the corresponding fragment of the sentence matrix. At the end, we obtain a scalar out of the sum of the product of each element in the filter with its corresponding portion of the sentence matrix. \autoref{filter} depicts the filter convolution operation of a $3x3$ sentence matrix and a $2x3$ kernel, resulting in a $1x2$ vector. The output in the image shows the concatenation of each resulting scalar. This scalar vector is what we call \textit{feature map}. A \ac{cnn} layer contains several repetition of a convolution with different filter sizes. The variation of the kernel's size has as purpose capturing different kinds of relevant information with each kernel. For instance, if we think about n-grams, a bi-gram expresses different knowledge than a tri-gram, therefore we slide distinctive filters over the same matrix. 

\begin{figure}[h]
	\begin{flushright}
			\includegraphics[scale=0.58]{pool}
	\end{flushright}
	\caption[Pooling operation]{Pooling operation, extracting the most representative information of the matrix.}
	\label{pooling}
\end{figure}

After obtaining the desired number of feature maps we apply a pooling strategy, which consists of extracting the most representative information of the resulting vector of the convolution. We can imagine that in the convolution we applied different filters, as mentioned before, obtaining our feature maps. Out of those vectors we perform a pooling operation obtaining a fixed-length vector that constitutes our sentence as a fixed lenght vector. \autoref{pooling} shows the pooling operation of three feature maps. It displays the extraction of the most relevant information from vectors with different sizes. This means, pulling out the element with the highest score in our feature map and concatenating that value into a vector so that we end up by having fixed-length vector representation of the current utterance.

\section{Probabilistic Graphical Models}

A probabilistic graphical model is an illustrative representation of a probability distribution, in which each node depicts a random variable \citep{Klinger2007}. Most probabilistic models can be classified into either \textit{generative} or \textit{discriminative}. Generative models calculate the joint probability $p(y,x)$ of a class $y$ and its features $x_{1:n}$. Commonly, people say that this kind of models \textit{tell a story} of how data was generated, which features define a class. In contrast to this, discriminative models concentrate rather on the prediction task. In other words, they tell the story of how likely is a class, given certain features, which can be also expressed as the conditional probability $p(y|x_{1:n})$ of the class $y$, given a set of features $x_{1:n}$. 

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{genvsdis1}
	\caption[Generative and discriminative models]{On the left side, an illustration of a Na\"{i}ve Bayes model, that calculates the joint probability of a label and a feature, by calculating the prior probability of a class $p(y)$ and the the conditional probability of a certain feature given a class $p(x_1|y)$. The right side on the contrary, shows a logistic regression model that calculates the conditional probability of a class given a set of features $P(y|x_1, x_2, x_3)$.}
	\label{gendis}
\end{figure}
\newpage
There is a wide variety of graphical models, \autoref{gendis} shows a Na\"{i}ve Bayes and a logistic regression classifier. Those two architectures are a general representation of generative and discriminative models respectively. However, they only predict one label at once, and in this thesis we want to be able to classify a sequence. Therefore, we present now probabilistic models that generate a sequence-label prediction for a given sequence input.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.55]{hmm}
	\caption{Illustration of a \acf{hmm}}
	\label{hmm}
\end{figure}

Hidden Markov models are a sequence version of a generative model. We can define them as a product over single Na\"{i}ve Bayes models, where a joint probability between a sequence input and its target label sequence is calculated \citep{Klinger2007}. In other words, it models a linear sequence of decisions. The big disadvantage of \acp{hmm} is that they are directed graphs, which means that the information flows only in one direction.


\subsection{Conditional Random Fields}\label{crfs}

\acp{crf} are arbitrary undirected graphical models introduced by \cite{Lafferty2001}. These models are trained in order to  maximize the conditional probability of the desired outputs given the corresponding inputs. They are probabilistic models that compute the conditional probability distribution of a sequence output $\vec{y}$ given a sequence input $\vec{x}$, also called observed sequence. In a general classification problem, we compute a \ac{crf} model by calculating how likely is an output given an input.
\begin{equation}\label{crfnormal}
	p(\vec{y}|\vec{x}) = \frac{1}{Z(\vec{x})}\prod(\phi(x,y))
\end{equation}

Another perspective for looking at \acp{crf} is as a factor graph. In this regard each factor $\phi$ determines the relationship of two nodes $\vec{x}, \vec{y}$. \autoref{crfnormal} illustrates how to model factors in a regular classification problem. We have to compute the conditional probability of a set of labels $\vec{y}$ given an observation set $\vec{x}$. Each factor represents the exponential function, which associated different features $f_i$ in $\vec{x}$ with the output $\vec{y}$ \citep{Klinger2007}. We calculate the product of each factor $\phi$ in the graph and normalize it. The normalization function is defined as follows:
\begin{equation}\label{norm}
	Z(\vec{x}) = \sum_{y'}\prod(\phi(x,y'))
\end{equation}

Summing over the product of all values of $\vec{x}$ for each $\vec{y'}$ as illustrated in \autoref{norm}.  However, in this thesis we want to compute the conditional probability distribution of a sequence of a current label and its predecessors, which is not taken into account in \autoref{crfnormal}. Following this idea, we need a \ac{crf} that includes preceding predictions. This type of \ac{crf} is called \textit{linear chain} \ac{crf}, which correspond to a conditionally trained finite-state machine \citep{Settles2004} and represent their output as a sequence, which is our goal. We define a \textit{linear chain} \ac{crf} in \autoref{crfchain}:
\begin{equation}\label{crfchain}
	p(\vec{y}|\vec{x}) = \frac{1}{Z(\vec{x})} exp\big(\sum_{i=0}^{n}\phi(y_i,x_i)+\sum_{i=0}^{n-1}\psi(y_i,y_{i-1}, x_i)\big),
\end{equation}

where we have an exponential function of two sums due to the exponent product rule. The first addition is a reformulation of the product in \autoref{crfnormal}. This part of the equation sums over factors $\phi$, telling us, how likely a label $\vec{y}_i$ is, given an observed $x_i$. The second addition operation goes over factors $\psi$, which compute how likely is that a label $y_i$ might appear after another label $y_{i-1}$. This latter sum, is parameterized as a matrix at position $y_{i}$, $y_{i-1}$. The whole exponential function is then normalized by a function $Z(\vec{x})$, which again is computed by summing all values of $\vec{x}$ for each $y_i$, being $n$ the maximal amount of elements in the sequence. \autoref{crf} presents a graphical depiction of a linear chain \ac{crf} including both type of factors, as are the relations between observation and prediction as well as between previous labels and the current one. To sum up, the key difference between \autoref{crfnormal} and \autoref{crfchain} is the inclusion of a sequence, in this case associating $y_i$ and $y_{i-1}$.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.35]{crf}
	\caption{Illustration of a \ac{crf} model}
	\label{crf}
\end{figure}


Now that we have a formal definition of \acp{crf}, we can turn back to our \ac{da} prediction problem, with $\vec{y}$ being a \ac{da} sequence and $\vec{x}$ the observed sequence of utterances from a dialogue. According to the previous definition, we can use \acp{crf} to include the history of previous predicted \ac{da}s in order to predict the current \ac{da}. This assumption rises the question of why we need a \ac{crf} model. In this regard, our aim of classifying a \ac{da} depending on earlier predictions can be achieved by a \ac{crf} architecture, taking into account that it models sequences. In addition to that, \acp{crf} allow information to flow in both directions. This leads to the fact, that a current label classification could affect a previous prediction depending on probabilities of both \acp{da}. This is important because it also helps to reduce the error rate of the model.

\subsection{Inference and Labeling}\label{viterbi}

After having our probabilities for each \ac{da} in our sequence we need to think about how can be possible for us to decode the best sequence. We already have conditional probabilities for each label $y_i$ given an input utterance $x_i$, but the question is how do we find the best path produced by our model. \cite{Rabiner1989} defines several problems when working with \acp{hmm} and we can adopt his theory for \acp{crf}. For instance, one of the issues he defines is the inference problem: given an observation sequence $\vec{x}$, how do we choose a label sequence $\vec{y}$ to be optimal in some meaningful sense. According to \cite{Rabiner1989}, there are several ways of solving this problem, namely finding the optimal label sequence that best matches the input sequence. The challenge here is how to define \textit{optimal}. One possible solution could be to concatenate together labels that are most likely in isolation. The problem with this solution is that even sequences that are not valid (having a transition probability $= 0$) would be accepted. This leads us to find another solution, for example maximizing the number of correct pairs and three-sequence-labels. This latter definition could be also a reasonable approach, however the most widely criterion used in this case is finding the best label path by applying the Viterbi Algorithm \citep{Rabiner1989}. The Viterbi Algorithm was introduced as an \textit{error-correction} mechanism for noisy digital communication links by Andrew Viterbi \citep{Sammut2011} and it is defined as follows:
\begin{equation}\label{viterbidef}
	\vec{y}*=\argmax_y P(\vec{y}|\vec{x})
\end{equation}

\autoref{viterbidef} can be understood as going over all possible sequences $\vec{y}*$ and choosing the most likely $\vec{y}$ for a given utterance sequence $\vec{x}$ \citep{Stolcke1998}. The idea of Viterbi is to maximize the probability of obtaining the entire \ac{da} sequence correct. \autoref{viterbigraph} shows a graphic representation of the Viterbi search for a \ac{da} sequence. We can think about this as walking forward and keeping track after each \ac{da} prediction, which was the path that led us to that label. At the end we walk backwards retrieving the most likely sequence as depicted in the image. The Viterbi algorithm is known as a decoding strategy, given that it recovers the most likely sequence by going backwards and choosing the most probable label at each point. In this regard, this explains how we are able to change previous predictions, which means, that while going backwards, Viterbi is able to change and might even change the preliminary classification of an utterance given its likelihood. Hence we mention that the current prediction might affect a previous one. 

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.52]{viterbi}
	\caption{Illustration of Viterbi decoding}
	\label{viterbigraph}
\end{figure}

\section{Related Work}\label{related}
Automatic classification of \ac{da}s has been addressed in many different ways, like \ac{hmm} \citep{Woszczyna1994}, probabilistic graphical models \citep{Kita1996, Ji2005},  language models \citep{Jurafsky1997} as well combining \ac{hmm}s with decision trees to include prosodic features \citep{Stolcke1998, Stolcke2000}. One important contribution in the research of spoken language was done by \cite{Godfrey1992}, collecting a corpus of telephone conversations and transcribing them manually. They gathered the \acf{swbd} (See \autoref{swbd} for a detailed description). This led \cite{Jurafsky1997} and \cite{Stolcke2000} to extend that corpus by segmenting and labeling it utterance-wise at a dialogue act level. That extension to the corpus opened the opportunity to do more research in the field, given that the resources were provided. This section contains some work already done in this field of \ac{da} classification.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{kalchbrenner}
	\caption{Architecture proposed by \cite{Kalchbrenner2013}}
	\label{kalch}
\end{figure}

This part of the section will be dedicated to describe the work that we will use as direct comparison to our proposed model. \cite{Reithinger1996} presented DAs prediction using language models. They used deleted interpolation to calculate the probability of sequences of DAs in the Verbmobil corpus, predicting 18 different DAs. Over 300 out of 1000 DAs were manually labeled and used as input data for their experiments. According to the authors, due to the structure of a dialogue, it can be considered almost identical to a language model. However, instead of predicting the next word, they predict the next act. They reported 40\% accuracy when predicting only one act and it increases to 75\% when predicting the top 3 of the DAs. With the same purpose, \cite{Geertzen2009} aimed to use stochastic context free grammars (SCFG) in order to extract the structure of the dialogue and determine what action to take at a state. In this case they used grammatical inference techniques to find structure in the sequential data. The proposed system takes the sequences, aligns them, and makes a comparison in order to find subsequences that could signal a structure. The output of the algorithm is a set of SCFGs, that are used to assign the next dialogue act. They compared the SCFG model with an n-gram model and found out that the SCFG model $(78.9\%)$ performed  $\approx12\%$ better than the n-gram model $(66.4\%)$ itself. 

More recently, people started applying deep learning techniques in the field of \ac{da} modeling. For instance \cite{Kalchbrenner2013} presented a joint model combining a sentence with a discourse model using hierarchical \ac{cnn} and combining them with \acp{rnn}, which they called RCNN. A diagram of this model is shown in \autoref{kalch}. One important characteristic of \acp{rnn} is that they include a memory cell, keeping previous relevant information. \cite{Kalchbrenner2013}'s idea is to obtain a vector representation from a hierarchical \acp{cnn} model and feed it into an \ac{rnn} layer. The one particular aspect from their approach is that they include the vector representation of the previous utterance into the memory cell of the \ac{rnn} in order to predict a current label. The authors report an accuracy of $73.9\%$ on the \ac{swbd}. 
 \begin{figure}[h]
 	\centering
 	\includegraphics[width=\textwidth]{lee}
 	\caption{Architecture proposed by \cite{Lee2016}}
 	\label{lee}
 \end{figure}
\cite{Lee2016} also attempted to solve \ac{da} classification by combining \acp{cnn} with a two layer feed-forward network. They also started with a vector representation  of the utterances, analog to \cite{Kalchbrenner2013}. The difference here is that they tried different approaches, namely \acp{cnn} and \acp{rnn} in order to obtain their fixed length vector. After that, they gave those vectors as input to a feed-forward neural network. However, they did not have a rigid model, but they tried different architectures as displayed in \autoref{lee}. One interesting point is that at the beginning they modeled \acp{da} with a stack of two feed-forward layers architecture. Although their statement was that sequential information did not help in most of the cases, the rest of their experiments include previous utterances. They report that their best performance was obtained with \acp{cnn} and their two-layer feed-forward model achieving $73.1\%$ accuracy on \ac{swbd} and $84.6\%$ on the \ac{icsi}. 
 
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.57]{ortega}
	\caption{Architecture proposed by \cite{Ortega2017}}
	\label{ortega}
\end{figure}
 Additionally, this year \cite{Ortega2017} presented a model using attention techniques and combining them with \acp{cnn} analog to \cite{Kalchbrenner2013,Lee2016}. This model combines \acp{cnn}, RNNs and Attention achieving $73.8\%$ on \ac{swbd} and $84.3\%$ on \ac{icsi} using two architectures. They obtain a fixed-vector representation for each utterance using \acp{cnn}. After that they apply two approaches, one is feeding the vectors into an LSTM and  applying a weighted sum to the output, which is the attention mechanism and delivers a vector representation as input for a softmax layer. On the other hand, they apply a weight to each vector representation, generating then the attention and providing them into an LSTM to produce again a distribution to be given into the softmax. This model is depicted in \autoref{ortega}.


