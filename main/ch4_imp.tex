\chapter{Dialog Act Classification Model}\label{impl}

This chapter contains the architecture of the proposed model in detail including the baseline that we take as a direct comparison of model performance. The model introduced in this thesis is an extension of the baseline and will be presented in \autoref{model_text}.


\section{Baseline}\label{baseline}

Since we use utterances' transcriptions as the input for our model, we can consider \ac{da} classification as a sentence classification problem. This leads us to the assumption that our aim of predicting \ac{da}s could be achieved extending \cite{Kim2014}’s proposed \ac{cnn} approach, as mentioned in \autoref{motiv}. The idea of this model is to adapt that model using \acp{cnn} to produce a vector representation of each utterance and passing it through a feed forward network and adding a softmax layer at the end in order to obtain a probability distribution over the \ac{da} set.

\begin{figure}
	\centering
	\includegraphics[scale=0.38]{mapping}
	\caption{Word indexes and embedding representation}
	\label{mapping}
\end{figure}

At the beginning we have a vector of word indexes that are mapped to its corresponding word embeddings inside the embedding layer as shown in \autoref{mapping}. This mapping operation is done in order to reduce the computational complexity of the model at the moment of loading the data and also because we are using pre-trained word embeddings. Otherwise we would have to load $205,000$ utterances and given that most of the words occur in several utterances, we would have an high amount of repeated word vectors each time we retrieve the data. Instead of doing that, we present each word as an index and link that number to its embedding. This embedding layer allows us to call each vector at processing time, without storing repeated data. Additionally, only a storage of indexes is needed, which takes less memory than loading the whole embeddings at once.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.47]{baseline}
	\caption{Baseline diagram}
	\label{fig:baseline}
\end{figure}

After using the embedding layer on the indexes input, we obtain a matrix representation of our sentence and pass it through a convolution layer. Here we have hundred filters. This filters or kernels have the same length as the embeddings, namely 300 dimensions. Regarding the height, we use 3, 4, and 5, which would be similar as having 3-grams, 4-grams and 5-grams. The idea of using \acp{cnn} is to analyze each utterance as a sequence of words and \acp{cnn} are a good tool to capture similarities and important properties of the words \citep{Goldberg2016}. The result of that convolution is a highly dimensional vector that is now the input of the global pooling layer, which has as goal to focus on the most important features regardless of their location \citep{Goldberg2017}. Regarding the pooling operation, depicted as \textit{Max pooling} in \autoref{fig:baseline}. At this point, it is relevant to take into account that the \ac{cnn} layer with variable-length input and filters of different shapes generates outputs without uniform shapes. The pooling operation is the one in charge of generating a  fixed-length representation for each utterance.


At this point of the process, we have a fixed-length vector representation of the utterance and use it as input for a softmax layer in order to calculate a normalized distribution over all classes. This is the last step of the model, as displayed in \autoref{fig:baseline}. In an ideal case, we expect that the highest value in the representation, delivered by the softmax, belongs to our target label. 


\section{Model Architecture}\label{model_text}
We now introduce our contribution and goal in this thesis, which consists of extending the model presented in \autoref{baseline}. Regarding the data, one important fact that we can observe, and that we already mentioned in \autoref{resources}, is that our corpora are imbalanced. In \ac{swbd}, $36\%$ of the utterances are labeled as a \textit{statement} and in \ac{icsi} $59\%$. Another relevant aspect is the context of each phrase; following our illustration with \textit{statement}, it is by far the most frequent class, followed by another \textit{statement} in both corpora. This feature of the corpora leads us to think about previous predictions or what we refer to as \textit{context}. However, it is not the only case, if we consider other \acp{da} that are less frequent, such as \textit{summarize / reformulate}, which is only $0.5\%$ of \ac{swbd}, we can also find a pattern. In most of the cases, speakers of \ac{swbd} decided to rephrase or summarize another utterance after a \textit{statement} or an \textit{agreement}. On the other hand, the most frequent \ac{da} following a \textit{reformulation} was an \textit{agreement}.

\begin{figure}[h!]
	\centering
	\resizebox{\textwidth}{!}{%
	\begin{tabular}{cll}
		\textbf{Speaker} &  \textbf{Utterance} & \textbf{Label} \\
		B & We came from nine years in a condo , uh                              & Statement     \\
		B & and working in that condo , you did n't have to do anything          & Statement     \\
		A & Uh-huh . you know , unless you really wanted to                      & Backchannel \\
		B & so                                                                   & Turn-Exit                 \\
		A & So now . you have the chance to really create and , and spread out . & Summarize     \\
		B & Right .                                                              & Agree            
	\end{tabular}
	}
	\caption[Dialogue example]{Example of a dialogue, which includes interlocutors, utterances and labels.}
	\label{table:motiv}
\end{figure}

\autoref{table:motiv} illustrates our previous definition. We can see that there are two consecutive \textit{statements} and, additionally, after a \textit{summarization} follows an \textit{agreement}. However, it can be also considered that there are some utterances in between that do not fit into the case previously described. These characteristics of the data are precisely what motivates our assumption of adding \acp{crf} techniques to this model. Looking at just one previous sentence is not enough, as presented in the table. If we want to know what is the speaker summarizing or reformulating, we cannot only consider an utterance that was interrupted (\textit{turn-exit}), we need to go further. Therefore, after showing this evidence from the data, we infer that previous utterances can contribute with useful cues and that earlier predictions deliver valuable information. In this model we combine \acp{cnn} with \ac{crf} techniques so that, we compute for each input utterance a label sequence taking into account prior decisions. In addition to that, we also include speaker information. Going back to the example in \autoref{table:motiv}, it presents that a \textit{statement} occurs after another \textit{statement}, when the speaker is the same one producing both. Following this intuition, we take into account who was the last interlocutor in the conversation and add it as a feature. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{model}
	\caption[Visualization of the proposed model]{Visualization of the proposed model. $U_t$ represents the current utterance and $y_t$ its associated \ac{da}. We illustrate the output of a linear layer as $x_{t-n}$ for a better explanation as a diagram. However, it should be understood as a vector representation.}
	\label{fig:model}
\end{figure}

\autoref{fig:model} illustrates our proposed model in detail. The input of this architecture is a concatenation of $n+1$ utterances as  with $n$ being the context length. In that particular example, we have a current utterance $U_t$ and its context $t=2$. This model takes a sequence of $n+1$ consecutive utterances as input, however, for didactic purposes, this example contains only $3$, represented  as $U_{t-2}, U_{t-1}$ and $U_{t}$.  In the same way, the target label sequence is shown in the image as $y_{t-2}, y_{t-1}$ and $y_{t}$.  Analog to our baseline we also perform a lookup of the embeddings and feed the current utterance and its $n$ preceding ones into the convolutional layer. Another distinction in this structure is that we perform an utterance-wise pooling. In other words, instead of performing a global max-pooling as we did in \autoref{baseline}, we extract the most relevant value of the feature map for each utterance. From this operation we obtain $n+1$ vector representations. We then add the speaker feature to each of those vectors as a binary element, being $1$ the current interlocutor, or $0$ otherwise. It is important to mention that the \ac{crf} model expects unnormalized scores for each class given the pooled vector. Therefore, we calculate those unnormalized scores for each \ac{da} with a linear layer. Having those scores, we compute transition probabilities for all possible label sequences of length $n+1$, given our current utterance and its predecessors. Additional to transitions we determine unary scores (this time normalized as shown in \autoref{crfchain}) for each \ac{da} given an input utterance. Calculating these likelihoods the models is almost done with the classification.

The last step in our proposed model is to evaluate it by decoding the most likely sequence using unary scores, which tell us how likely is a \ac{da} given the vector representation of an input utterance. Besides, we need the cost for each label sequence, which is the transition probability given the whole sequence. Using the Viterbi algorithm we are able to decode the most likely sequence of \ac{da}s as illustrated previously in \autoref{viterbigraph}. In addition, we effectively improve the performance on \ac{da} classification with this architecture compared to the previous model.

