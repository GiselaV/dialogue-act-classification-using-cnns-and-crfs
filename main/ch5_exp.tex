\chapter{Experiments}\label{exp}
  
The main objective of this thesis is to be able of classifying automatically \ac{da}s using \ac{dl} techniques. Following this idea, we now present all our experiments, including preprocessing the data, libraries used in this project and setup of our model. Subsequently, after each experiment, we will illustrate in this chapter our experimental results. 

\section{Data Preprocessing}\label{prepro}
In order to achieve our goal, we trained the suggested model in \autoref{model_text} on the corpora introduced in \autoref{data}, following the same splits used by  \cite{Lee2016}\footnote{\url{https://github.com/Franck-Dernoncourt/naacl2016}}. However, although the data is already annotated, it needs certain adjustment to fit the requirements of the model. 


Firstly, we extracted all utterances and cleaned them, deleting unusual symbols and extra spaces. Additional to that, we transformed the utterances to lower case. On the other hand, we mapped each of the pre-trained word vectors (embeddings) from w2v and GloVe to an index. Subsequently, we took each utterance and its five previous expressions with the purpose of storing them together. These utterances were stored in the form of indexes, which enables us to link each utterance to its corresponding word vector. Going back to the context, we already mentioned that we keep previous utterances. However, achieving this is not possible for the first five utterances in our dialogue, because there is not previous context. In order to solve this issue, we created an extra class allowing in this way, to provide a context at the beginning of each dialogue too. Adding an extra class and its corresponding \textit{No-text}-utterance is what we call padding. As for padding, there is another issue that we identified, namely the variety in the length of each utterance. This problem was solved at the moment of keeping indexes for every word in an utterance by adding as many zeros as necessary to match the length of the longest utterance in the corpus. This operation is also called padding. An example of this process is presented in \autoref{dummy_text}, showing the interlocutor, the text contained in each sentence and its associated label. It can be also observed, that we converted the interlocutor into a binary element, where [1] represents the current speaker also in previous expressions and 0 any prolocutor. 

\definecolor{LightCyan}{rgb}{0.8,0.95,1}
\begin{figure}[h!]
	\centering
	\begin{tabular}{cll}
		\textbf{Speaker} & \textbf{Utterance}  & \textbf{Label} \\
		$[0]$ & \textit{No text}				   & None \\
		$[1]$ & Do you have any pets?                  & Yes-No-Question \\
		$[0]$ & Yes,                                   & Yes answer \\
		$[0]$ & I have a dog and cat now.              & Statement-non-opinion \\
		\rowcolor{LightCyan}
		$[1]$ &  Oh, what are their names?              &  Wh-Question \\
	\end{tabular}
	\caption[Representation of a current utterance and its predecessors]{Example of a complete training instance, which includes speaker, utterance and label, for a current utterance (highlighted in blue) and its predecessors.  As depicted in the table, the current speaker will be always $[1]$. The extra created utterance for padding is represented with \textit{"No text"} and the label \textit{None}.}
	\label{dummy_text}
\end{figure}

\section{Software Overview}\label{software}

The model presented in this thesis was implemented in Python 2.7 and the 1.2.1 GPU version of Google's TensorFlow available in the \textit{Python Package Index}\footnote{\url{https://pypi.python.org/pypi/tensorflow-gpu/1.2.1}} \citep{Tensorflow2015}. TensorFlow is an open-source library implemented for mathematical calculations. It gained relevance in the \ac{ml} field, given that it contains several already implemented functions that allow easily to work with neural network models, but not restricted to them. Our baseline (see \autoref{baseline}) was inspired on the model proposed by \cite{Kim2014} and more specific it is an extension of its TensorFlow version \citep{Britz2015}. We adapted that model to perform \ac{da} classification, because it was designed for sentiment analysis, and extended it using the TensorFlow  implementation of \ac{crf} \citep{Tensorflow2015}\footnote{\url{https://github.com/tensorflow/tensorflow/tree/master/tensorflow/contrib/crf}}. As a result, our model is composed by the following layers: embeddings, convolutional including max-pooling, a linear layer and a \ac{crf}, all together illustrated in \autoref{fig:model}.

\section{Setup and Results}\label{results}

In this section we first present the hyperparameters we used to run our model before we introduce experimental results. Those hyperparameters are shown in  \autoref{table:hyper}, we adopted most of them from \cite{Ortega2017}'s work: dropout probability, dimensions of the embeddings, sizes of the filters and the number of filters. Contrasting this we also tried some fine tuning on the others. The tuning on the parameters will be introduced later in this section.

\begin{table}[h!]
	\centering
	\begin{tabular}{@{}lll@{}}
		\toprule
		Hyperparameter & \ac{swbd} & \ac{icsi} \\ \midrule
		Context & \multicolumn{2}{c}{$[1,2,3,4,5]$} \\
		Dropout probability & \multicolumn{2}{c}{0.5} \\
		Embeddings dimensionality & \multicolumn{2}{c}{300} \\
		Filter sizes & \multicolumn{2}{c}{3, 4, 5} \\
		Learning rate &\multicolumn{2}{c}{0.001} \\
		Mini-batch size & 150 & 50 \\
		Number of epochs & \multicolumn{2}{c}{10} \\
		Number of filters & \multicolumn{2}{c}{100} \\
		Optimizer & \multicolumn{2}{c}{Adam} \\ \bottomrule
	\end{tabular}
	\caption{Initial hyperparameters used to run our first model}
	\label{table:hyper}
\end{table}



\subsection{Baseline}\label{base}
Regarding our experiments, we firstly ran our baseline model using the hyperparameters depicted in \autoref{table:hyper}. We only changed the context and the rest of the parameters were adopted as presented. We decided to run our baseline only in context $1$ and $2$ and also without context. The accuracy achieved with this architecture is displayed in \autoref{rls:base}. We can observe that there is a significant difference between attempts including context and the one without. Another aspect that can be identified is the difference in performance depending on the pre-trained word embeddings. In general we can state that our model benefits more from w2v than from GloVe, achieving therefore a higher accuracy. 


\begin{table}[h!]
	\centering
\begin{tabular}{lllll}
	& \multicolumn{2}{c}{SwDA} & \multicolumn{2}{c}{MRDA} \\ \hline
	\multicolumn{1}{|l|}{Context} & \multicolumn{1}{l|}{w2v} & \multicolumn{1}{l|}{GloVe} & \multicolumn{1}{l|}{w2v} & \multicolumn{1}{l|}{GloVe} \\ \hline
	\multicolumn{1}{|l|}{0} & \multicolumn{1}{l|}{71.0} & \multicolumn{1}{l|}{70.3} & \multicolumn{1}{l|}{80.9} & \multicolumn{1}{l|}{80.5} \\ \hline
	\multicolumn{1}{|l|}{1} & \multicolumn{1}{l|}{55.2} & \multicolumn{1}{l|}{54.5} & \multicolumn{1}{l|}{65.8} & \multicolumn{1}{l|}{60.2} \\ \hline
	\multicolumn{1}{|l|}{2} & \multicolumn{1}{l|}{46.9} & \multicolumn{1}{l|}{46.7} & \multicolumn{1}{l|}{51.2} & \multicolumn{1}{l|}{56.7} \\ \hline
\end{tabular}	
	\caption{Accuracy reported on our baseline for both corpora \ac{swbd} and \ac{icsi}}
	\label{rls:base}
\end{table}


Another substantial point about this model is regarding the pooling operation described in \autoref{baseline}. Max pooling in this model is performed globally. This affects the performance, given that extracting only one scalar out of the feature map obtained from the concatenation of two utterances results on ignoring relevant information contained in one of the utterances. This phenomenon occurs also for any other attempt using context higher than zero. As a conclusion, we can state that the higher the context, the worst this model performs due to the global max pooling operation. Summing up, our baseline model achieves its highest accuracy when performing without context and with w2v being its best performance $71.0\%$ for \ac{swbd} and $80.3\%$ on \ac{icsi}.

\subsection{CNN-CRF-Model}\label{rsl:joint}

Now we come to our proposed model. At this point, experiments were executed with the architecture depicted in \autoref{fig:model} and the hyperparameters introduced in \autoref{table:hyper}. Results for this procedure are presented in \autoref{table:joint}. Here we can observe that comparable to the baseline, the performance of this model is approximately $2-3\%$ higher. It can be noted that the best accuracy on \ac{swbd} was performed using $2$ previous utterances on w2v, whereas the model using GloVe needed $5$  to achieve the best outcome on the corpus. 

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|l|l|l|l|}
		\multicolumn{1}{l}{}&\multicolumn{2}{c}{SwDA} & \multicolumn{2}{c}{MRDA} \\ \hline
		Context      & w2v       & GloVe       & w2v        & GloVe \\ \hline
		1            & 71.3      & 72.8        & 82.8       & 82.6  \\ \hline
		2            & \textbf{72.7}      & 72.8        & 82.8       & \textbf{83.0}   \\ \hline
		3            & 71.7      & 72.5        & \textbf{82.9}      & 82.6  \\ \hline
		4            & 72.1      & 72.1        & 82.6       & 82.4  \\ \hline
		5            & 72.1      & \textbf{73.1}        & 82.5       & 82.7 \\ \hline
	\end{tabular}
	\caption[Accuracy ($\%$) of the \ac{cnn}-\ac{crf} joint model]{Accuracy ($\%$) of the \ac{cnn}-\ac{crf} joint model. Numbers in bold represent the highest performance in each case.}
	\label{table:joint}
\end{table}

The model reaches an accuracy of $72.7\%$ using w2v and $73.1\%$ using GloVe on \ac{swbd}. In contrast to this, the model was able to reach its best result using $3$ and $2$ previous sentences on w2v and GloVe respectively, when tested on \ac{icsi}. It is meaningful to consider that these numbers are produced by the model without further parameter tuning. However, this model already shows an improvement in terms of correctness compared to our baseline, see \autoref{disc} for a deeper discussion about this. One interesting aspect with this results is that we are not able to recognize any pattern. At this point we cannot give any statement on how long should be the context, but what we can conclude now is that the model performs better using GloVe as pre-trained word embeddings.


\subsection{Extended CNN-CRF-Model}\label{extmodel}
After considering the outcome of our proposed model, we decided to include the current interlocutor as feature into our architecture. In \autoref{graph:spkr}, we display how we concatenated a binary element to the fixed-vector representation of the utterance before feeding it into the \ac{crf}. That binary feature was defined to be $1$ for the current interlocutor and $0$ otherwise. This means, that the trained model could be able to identify previous utterances said by our current speaker. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth]{spkr}
	\caption[Speaker feature visualization]{Visualization of the speaker feature as a binary element concatenated to the fixed-vector representation obtained after the max-pooling operation.}
	\label{graph:spkr}
\end{figure}

In \autoref{table:spkr}, we present the accuracy of this extension.  Although we were expecting to boost the performance of the model by adding this feature, it only improved slightly. However, what we can recognize from these results is the performance regarding the context. We could dare to state that the best context for this architecture is between two and three previous utterances. Another fact in these numbers is that the accuracy is quite similar among all presented cases. The best accuracy reported on \ac{icsi} only differs in the context, but the highest performances are the same regardless the of pre-trained embeddings. On the other hand, the results on \ac{swbd} do have a difference. The model performs better using GloVe and its highest accuracy is achieved taking into account two previous utterances.

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|l|l|l|l|}
		\multicolumn{1}{l}{}&\multicolumn{2}{c}{SwDA} & \multicolumn{2}{c}{MRDA} \\ \hline
		Context      & w2v       & GloVe       & w2v        & GloVe \\ \hline
		1            & 72.6      & 72.4        & 82.6       & 82.7  \\ \hline
		2            & 71.9      & \textbf{73.2}        & \textbf{83.1}       & 82.6  \\ \hline
		3            & \textbf{72.9}      & 72.5        & 82.7       & \textbf{83.1}  \\ \hline
		4            & 72.4      & 72.6        & 82.7       & 83.1  \\ \hline
		5            & 72.3      & 73.0          & 82.4       & 82.5   \\ \hline
	\end{tabular}
	\caption[Accuracy achieved by our joint model \ac{cnn}-\ac{crf} + speaker]{Accuracy achieved by our joint model \ac{cnn}-\ac{crf} after including speaker feature}
	\label{table:spkr}
\end{table}

However, at this point we still believe that this performance could be improved. Hence, we decided to conduct some parameter tuning and to analyze the outcome. For instance, we attempted to find the best learning rate and the best optimizer by running our model on several optimizers adapting the learning rate with step size 0.001 in interval $[0.001, 0.009]$, step size $0.01$ in interval $[0.01, 0.09]$ and step size $0.1$ in interval $[0.1, 0.9]$, which results in 27 different learning rates tested.  Following this idea, we called our next step \textit{optimizer search}. This experiment consists on running our model employing different optimizers:  Adam, Gradient Descent Optimizer (GDO), Adadelta, Adagrad and Gradient Descent Momentum and using the mentioned learning rates. It is important to know, that some of these optimizers work under an adaptive algorithm, such as Adagrad,  adaptive gradient algorithm \cite{Duchi2011} or Adam, which modify the learning rate along the training process. In those cases our tested learning rate value is only the start value.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.69]{lr}
	\caption[Optimizer search visualization]{Optimizer search visualization, executed in the interval $[0.001, 0.009]$ with step size $0.001$, interval $[0.01, 0.09]$ and step size $0.01$ and finally in interval $[0.1, 0.9]$ and step size $0.1$}
	\label{lr}
\end{figure}

\autoref{lr} shows the outcome of the optimizer search. It is difficult to recognize which values are the best as a learning rate, given that several optimizers perform quite similar. What we can easily notice is that the majority of the optimizers achieved a good performance between $0.02$ and $0.5$. However, it is possible to perceive a little difference on the performance of Adagrad and GDO. In this regard, we ran our model employing three optimizer functions Adagrad, GDO, and Adam. This latter was already in our setup, therefore we continued testing on it. We used context $t=2$ on \ac{swbd} and w2v as pre-trained embeddings. The selected context $t=2$ was due to it high performance on previous experiments. In the same way, we ran this optimizer search for \ac{icsi} too, but this time, we narrowed our research to those three optimizers and increased the context to $t=5$. The reason for incrementing the amount of previous utterances was that for in our first test we used the number of context utterances producing the best performance, in this case $t=2$. However, at the moment of testing the model on all other possible context values, the accuracy always decreased being the highest context one of the lowest performances. This brought us to take the highest number of previous utterances in this try, given that it would be in our opinion the lowest one avoiding any posterior decay. In addition to that and according to \cite{Smith2017}'s approach, we decided to increase the mini-batch size too. Their approach was to increase the mini-batch size automatically during training. Contrasting their idea, we decided to keep the size fix during training but we did increased it on different attempts. As a result, we also found the most appropriate batch size for our model, which is $170$ and $70$ utterances on \ac{swbd} and \ac{icsi} respectively . 



As presented in \autoref{table:tuned}, our best accuracy was achieved updating the optimizer, learning rate and mini-batch size. All those parameters allow us to reache a good performance on both corpora. In comparison to our previous setup, we updated the learning rate from 0.001  to 0.07 for \ac{swbd} and 0.01 for \ac{icsi}. We are using now 2 optimizers instead of only one. Adagrad on \ac{swbd} and Gradient Descent on \ac{icsi}. Finally, we also found an optimal mini-batch size being $70$ for \ac{icsi} and $170$ for \ac{swbd}. Using these setup we run our last experiments with our proposed extended model.

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		\textbf{Tuned Parameters} & \textbf{SwDA} & \textbf{MRDA}    \\ \hline
		Learning rate             & 0.07          & 0.01             \\ \hline
		Mini-batch Size           & 170           & 70               \\ \hline
		Optimizer                 & Adagrad       & Gradient Descent \\ \hline
	\end{tabular}
	\caption{Updated hyperparameters after performing an optimizer tuning}
	\label{table:tuned}
\end{table}

Lastly, we present accuracy reached by our \ac{cnncrf} model including the speaker feature and also after fine tuning the hyperparameters. On the \ac{swbd} corpus the best accuracy is obtained processing only one previous utterance using w2v, while using GloVe it managed to produce its best numbers taking into account two utterances as context. Contrasting this, on \ac{icsi}, this model achieved its best results both times using five previous utterances, which partially confirms our hypothesis of using \ac{crf} for this task. In \autoref{disc} we discuss this more concretely. In addition to that, this time it is again interesting to take a look at the pre-trained embeddings. While most of our previous models reached their best performance using GloVe on both corpora, the present model is achieving its best accuracy using w2v on both corpora. We report on \ac{swbd} an accuracy of  $74.1\%$ using w2v and 1 previous utterance and $73.7\%$ using GloVe and 2 utterance from context. On the other hand, our model accomplishes an accuracy of $85.5\%$ using w2v and  $84.7\%$ using GloVe, both with a context of 5 utterances. This results are $1\%$ higher than our first model and $3\%$ over our baseline on \ac{swbd}. While on \ac{icsi} our current model performs $2\%$ better than our first implementation and over $5\%$ better than the baseline.

\begin{table}[h!]
	\centering
	\begin{tabular}{|c|l|l|l|l|}
		\multicolumn{1}{l}{}& \multicolumn{2}{c}{\textbf{\ac{swbd}}} & 
		\multicolumn{2}{c}{\textbf{\ac{icsi}}}\\	\hline
		\textbf{Context} & \textbf{w2v} & \textbf{GloVe} & \textbf{w2v} & \textbf{GloVe} \\ \hline
		1 & \textbf{74.1} & 72.4 & 84.9 & 84.1 \\ \hline
		2 & 73.8 & \textbf{73.7} & 85.2 & 84.4 \\ \hline
		3 & 73.6 & 73.4 & 85.2 & 84.6 \\ \hline
		4 & 73.5 & 73.0 & 85.3 & 84.7 \\ \hline
		5 & 72.8 & 73.3 & \textbf{85.5} & \textbf{84.7} \\ \hline
	\end{tabular}
	\caption[Performance of proposed  model including tuning]{Performance of the model including the speaker feature and hyperparameter tuning for both corpora \ac{swbd} and \ac{icsi}}
	\label{rls:spk}
\end{table}

Reaching this performance is satisfactory, but it is also necessary to report an accuracy in average, in order to proof that the result was not by chance, but the model does reach certain success. This current architecture, which includes hyperparameter tuning and the speaker feature was evaluated on five times in order to report an intermediate accuracy over all attempts. Since the model produces a higher accuracy on w2v, this procedure was conducted only on w2v, for both corpora. \autoref{average} displays an intermediate score for each context after five runs each of them including $30$ epochs. It also contains the minimum and maximum performance of the model in both corpora. 

\begin{table}[h!]
	\centering
	\begin{tabular}{|c|r|r|}
		\hline
		\textbf{Context} & \multicolumn{1}{c|}{\textbf{MRDA}} & \multicolumn{1}{c|}{\textbf{Swbd}} \\ \hline
		1 & 84.9 (84.6, 85.0) & 73.4 (73.1, 73.6) \\ \hline
		2 & 85.2 (85.0, 85.3) & 73.7 (73.3, 74.3) \\ \hline
		3 & 85.2 (85.2, 85.4) & 73.7 (73.1, 74.2) \\ \hline
		4 & 85.3 (85.2, 85.5) & 73.6 (73.1, 74.0) \\ \hline
		5 & \textbf{85.3} (85.1, 85.5) & \textbf{73.9} (73.1, 74.6) \\ \hline
	\end{tabular}
	\caption[Averaged performance of the tuned model]{Accuracy (\%) on contexts form 1 to 5. We report an average (minimum, maximum) calculated on $5$ runs. On both corpora the model reaches its highest performance by taking $5$ previous utterances and their predictions }
	\label{average}
\end{table}

\autoref{average} presents an average over all contexts for both corpora. It is noticeable that the present framework achieves its best accuracy taking as input five previous utterances and the current one, obtaining in average $85.3\%$ and $73.9\%$ for \ac{icsi} and \ac{swbd} respectevely. Comparing these numbers to the baseline presented in \autoref{baseline}, we can see that our model achieves a maximum accuracy of $85.5\%$ compared to $80.9\%$ of the baseline when tested on \ac{icsi}. Additionally, we report $74.6\%$ of accuracy in comparison to a baseline that achieves $71.0$. This demonstrates that in regard to the highest accuracy of the model, it outperforms  in both cases  the baseline the by over $5\%$ and $3\%$ on \ac{icsi} and \ac{swbd} correspondingly.