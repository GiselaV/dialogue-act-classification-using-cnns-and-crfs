\chapter{Analysis}\label{disc}
This part of the thesis comprises a discussion regarding the results presented in \autoref{exp}. We analyze all three models and their performance in both corpora using both word embeddings.

\section{Baseline}

The numbers shown in \autoref{rls:base} display a model, whose best achievement is on isolated utterances. This might be controversial, given that the suggested model in this thesis to approach \ac{da} modeling is inspired in the sequence classification. Nonetheless, there is enough evidence in previous work that \acp{cnn} are a good strategy to address our task \citep{Lee2016, Kim2014}. 

In this experiment it is relevant to recall the max-pooling process, where the most representative information is extracted into a vector. This process might be risky, taking into account that this pooling is done globally we might loose valuable information. Extracting the most relevant information out of two or three sentences would lead to extract only one feature, while the number of extracted features should be equal to the number of sentences that are taken into consideration, only that way the context would make sense. Despite this limitation, the baseline produces $71.0\%$  of accuracy in a corpus that has $84\%$ in manual annotation agreement, which is something that can be exceeded, but it is much higher than a random or a majority class baseline which is $59.1\%$ in \ac{icsi} and $33.7\%$ in \ac{swbd}.

\section{CNN-CRF-Model}

This model already produces results about $2\%$ higher accuracy than the baseline using either of both pre-trained embeddings. This improvement shows, in our opinion, that the use of \acp{crf} is an accurate strategy, resulting in an increment of the performance. Considering the example in \autoref{yes_question}, we can observe that a \textit{yes-answer} \ac{da} may not contain major information other than one or two words and punctuation. The big advantage in this case is that this class has often the same answer, but not always. 

\begin{figure}[h!]
	\centering
	\begin{tabular}{cll}
		\textbf{Speaker} & \textbf{Utterance}  & \textbf{Label} \\
		B & QUANTUM LEAP? & Declarative Yes-No-Question \\
		A & Uh-huh.	& Yes answer                     \\
		B & Did you see it the other night? & Yes-No-Question   \\
		A & Um, uh-huh.                     & Yes answer        \\
		B & That was, that was pretty good. & Statement-opinion \\
		A & Yeah                            & Agree/Accept     
	\end{tabular}
	\caption[Example of ambiguities in \acs{swbd}]{Segment of a dialogue taken from \ac{swbd}, which provides evidence of ambiguities in the corpus.}
	\label{yes_question}
\end{figure}

\autoref{yes_question} introduces a conversation where the expression \textit{Uh-huh} was considered a \textit{Yes answer}. However, there is a major problem here, namely that \textit{Uh-huh} can be also labeled as a \textit{backchannel} as shown in \autoref{sw-tags}. In addition, this is not an isolated example, the utterance labeled as an \textit{agreement} can be labeled as \textit{yes-answer} too. This raises the question of how to tell the model to differentiate between them. A good solution would be to include more information, which is exactly what we are doing by adding a \ac{crf} on top of the baseline. We add context cues that helps the model to understand that after a \textit{question} comes an \textit{answer} and in case we do not have a preceding \textit{question}, then the decision should be another \ac{da} e.g. \textit{agreement}.

The remarkable aspect about its performance is that it has no fine tuning as it simply puts all components together, adopting the hyperparameters from previous work and letting it run. However, it is interesting that the accuracy does not consistently increase or decrease depending on any factor, or at least we could not find any consistent pattern. It reaches its  best performance results for different context values across all cases (corpora and pre-trained embeddings). This leads to difficulties at the moment of analyzing which context or word embeddings enable the best performance. A reason that could explain this would be the lack of tuning, because the model indeed shows a learning process. However, its divergent behavior suggests that its hyperparameters should be adjusted in order to make it more profitable.



\section{Extended CNN-CRF-Model}

This model, which includes a speaker feature and fine tuning on the optimizer, learning rate and mini-batch size achieves the best results reported in this thesis. It evidences the effect of using previous utterances and their predictions comprised into a \acp{crf} layer. Although its operation on \ac{swbd} does not reflect our hypothesis of solving \ac{da} modeling as a sequence problem, the classification produced on \ac{icsi} actually shows the increasing performance on each longer context. This demonstrates that \acp{crf} indeed are an efficient approach to address the task of dialogue act modeling. 

At this point, there are several questions to be answered. For instance, \acp{crf} boost the performance more on \ac{icsi} in comparison to \ac{swbd}. This rises the question of what could be an explanation for this effect. In our opinion, the multi-party dialogues in \ac{icsi} play an important role on this phenomenon. For instance, when someone asks a question, it might be followed from several answers due to more than two people in the conversation. In this regard, it is necessary to take into a count a context of several sentences in order to obtain more relevant information about the current \ac{da}. Another interesting case, which is present in all experiments is the different performance of the models depending on the pre-trained word embeddings (w2v and GloVe) used. We might remember from \autoref{emb}, that those word representations are generated with different mechanisms and our model benefits more from the strategy of vectors that were trained using the \textit{Skip-gram model}, which as previously mentioned predicts a word based on another word in the same sentence.

A relevant point after observing the results presented in \autoref{results} is that an average of the experiments is highly recommended. We report at the beginning on one run-result for each experiment. However, succeeding our last experiment, we ran the model five times using the same setup with the purpose of calculating an accuracy mean. This procedure proofs that the model is not acting by chance and that its best performance can be kept during several runs. This results confirm, this time completely our hypothesis that taking context into account is an efficient way of approaching \acp{da} classification.

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|r|r|}
		\hline
		\multicolumn{1}{|c|}{\textbf{Model}} & \multicolumn{1}{c|}{\textbf{SwDA}} & \multicolumn{1}{c|}{\textbf{MRDA}} \\ \hline
		CNN-FF & 73.1 & 84.6 \\ \hline
		LSTM + Attention &73.8 & 83.3 \\ \hline
		RCNN & 73.9 & - \\ \noalign{\hrule height 1.5pt}
		Baseline & 71.0 & 82.7 \\ \hline
		\textbf{CNN+CRF} & \textbf{74.6} & \textbf{85.5} \\  
		\noalign{\hrule height 1.5pt}
	\end{tabular}
	\caption{Accuracy (\%) of our models compared to previous approaches}
	\label{comparison}
\end{table}

Finally, demonstrating that the present architecture greatly surpasses the presented baseline shows a relevant improvement. However, we need to compare our model to other works in the field and make sure that the performance is as good as we are expecting. Therefore, we now present that comparison. In \autoref{related}, we referenced previous work in the field of \ac{da} modeling and mentioned that we analyze our system in regard to \cite{Kalchbrenner2013}, \cite{Lee2016} and \cite{Ortega2017}. \autoref{comparison} shows the accuracy in percentage produced by our model contrasted to their work. We report almost $\approx 1\%$ higher performance than  \cite{Kalchbrenner2013}'s CNN-FF model and \cite{Ortega2017}'s LSTM + Attention model on \ac{icsi}. In addition to that, it also performs $\approx 2\%$ higher accuracy than \cite{Ortega2017}'s LSTM + Attention model and slightly better ($0.2$) than \cite{Lee2016}'s RCNN method on \ac{swbd}. For us, this is the evidence, that \acp{crf} are indeed a good approach to solve classification of \acp{da}. The reason for that is that we are taking into account not only features and additional other information from previous sentences, but we are also considering previous predictions by means of a \ac{crf} model and it is, in our opinion, what other methods are missing.

