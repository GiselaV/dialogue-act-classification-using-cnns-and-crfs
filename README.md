# README #

This repository contains the documentation of my M.Sc. graduation project. A pdf version of it can be found in the my_thesis.pdf file.

For this thesis I am using the EPFL template provided at overleaf.com

The code implementation of the classifier is here: https://bitbucket.org/GiselaV/dialog-act_cnn-crf-classifier/

### Abstract ###
Understanding spoken language is not a trivial task due to the wide variety of ambiguities. 
It is challenging for a system to differentiate between recognize speech and wreck a nice beach. 
However, research in this field is focusing on other aspects beyond only recognizing words from a signal or converting text into speech. 
Nowadays, it is necessary to develop systems that are able to do more, like establishing a whole conversation with the user or summarizing an entire dialogue in a meeting. 
Researchers argue that the first step to recognize spontaneous speech is to determine the structure of discourse. 
In other words, being able to identify the intention or the function of each utterance in a dialogue.

This thesis explores the automatic classification of Dialogue Acts (DAs) in a spontaneous conversation. 
The model proposed in this document addresses the problem as a sequence labeling task. 
We present a model that combines Convolutional Neural Networks (CNNs) and Conditional Random Fields (CRFs) in order to perform the sequential classification. 
CNNs generate a fixed-length vector representation of each utterance in the dialogue and CRFs deal with the problem of finding the most likely DA sequence for a sequence of input expressions. 
The suggested model is evaluated in two benchmarks outperforming previous works done in this field.